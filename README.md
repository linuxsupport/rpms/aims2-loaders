**Please note this repo is no longer in use as we only keep our loaders in our `/aims_share` Ceph share. You can still keep this rpm's content as a source of truth, but it will need to be added as well to the share directly.**

**MOVED TO https://gitlab.cern.ch/linuxsupport/aims/aims-loaders**

# PXE boot loaders package for aims2-server

`hwreg` directory is for unregistered interfaces. Bootloaders will use predefined images for auto-registration.

`aims` directory is for registered interfaces.

# Notes

We now use HTTP as the download protocol for kernel images instead of TFTP to speed up download time. 

This implies that Apache configuration from <https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims> is now really important.
