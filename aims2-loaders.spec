%define _binaries_in_noarch_packages_terminate_build 0
Name: aims2-loaders
Version: 2.8.37
Release: 1%{?dist}
Summary: AIMS2 - Loaders - PXE Loaders for Automated Installation Management Server (v.2)
Group: System Environment/Daemons
Source0: %{name}-%{version}.tgz
License: GPL
Vendor: CERN
URL: http://cern.ch/linux/
BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch
Requires: tftp-server
Requires: aims2-server
Requires: sed
Requires: bash
Requires: grep

%description
AIMS2 : Automated Installation Management Server (v.2) Boot Loaders and menus
Documentation at: http://twiki.cern.ch/twiki/bin/view/LinuxSupport/Aims2

%prep

%setup -q

%build

%install
mkdir -p $RPM_BUILD_ROOT/

make install DESTDIR=$RPM_BUILD_ROOT/
%clean
rm -rf $RPM_BUILD_ROOT

%post
[ -r /etc/aims2.conf ] && . /etc/aims2.conf || :
if [ "x$AimsServer" != "x" ];then
/usr/sbin/%{name}-tweak check $AimsServer
[ $? -ne 0 ] &&  /usr/sbin/%{name}-tweak set $AimsServer || :
fi


%files
%defattr(-, root, root)
/tftpboot/*
%attr(0755, root, root) /usr/sbin/%{name}-tweak

%changelog

* Thu Jan 28 2021 Daniel Juarez <djuarezg@cern.ch> - 2.8.37.1
- ARM64 no longer needs custom quit command. exit will do

* Wed Jan 27 2021 Daniel Juarez <djuarezg@cern.ch> - 2.8.36.1
- Use linuxsoft for default c8 ks, which is accessible from the TN

* Tue Jan 12 2021 Daniel Juarez <djuarezg@cern.ch> - 2.8.35.1
- Upgrade grub2 image for ARM

* Mon Nov 30 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.34.1
- OTG0054345: SLC6 is now unsupported

* Tue Nov 17 2020 Daniel Abad <d.abad@cern.ch> - 2.8.33.1
- Use linuxefi in all cases except for SLC6 (too old kernel)
- Use the same GRUB2 image for HWREG as for normal installations

* Mon Nov 16 2020 Daniel Abad <d.abad@cern.ch> - 2.8.32.1
- add CC7.9 to BE/CO menu

* Mon Nov 16 2020 Daniel Abad <d.abad@cern.ch> - 2.8.31.1
- add CC7.9 PROD

* Fri Nov 13 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.30.1
- Build for C8

* Thu Nov 12 2020 Daniel Abad <d.abad@cern.ch> - 2.8.29.1
- add CC7.9 TEST

* Fri Nov 6 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.28.1
- add RHEL 8.3 menus

* Tue Nov 3 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.27.1
- Fix GRUB2 theme

* Fri Oct 23 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.26.1
- Use grub2-2.02-0.86.el7.centos.src.rpm for uefi loader
- Add Sleep patch to detect more key return codes
- Fix source rpm code so it does not reboot on grub_exit

* Thu Oct 15 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.25.1
- Minor fixes in the GRUB2 image for uefi machines

* Wed Oct 14 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.24.1
- Temporary theme removal for grub2 as fonts seem broken

* Mon Oct 05 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.23.1
- Update grub image versions for uefi to 2.04

* Wed Sep 30 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.22.1
- add RHEL 7.9 menus

* Wed Aug 26 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.21.1
- Change menu entries into HTTP to speed up

* Wed Aug 26 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.20.1
- Add UEFI rescue entries

* Tue Aug 25 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.19.1
- Fix UEFI netbootxyz menu entry

* Wed Aug 19 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.18.1
- update BE/CO UEFI menu to install C82 instead of C81 

* Tue Jul 14 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.17.1
- add NetbootXYZ chainloading to BIOS and GRUB menus

* Thu Jul 02 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 2.8.16-1
- add entries for C8.1 and C8.2

* Fri May 29 2020 Ulrich Schwickerath <ulrich.schwickerath@cern.ch> - 2.8.15-1
- correct base OS repository for C8 interactive installs

* Thu Apr 30 2020 Ben Morrice <ben.morrice@cern.ch> - 2.8.14-1
- add fedora 3[1-2] menus
- add rhel 8.2 menus
- change CC7.8 menus to reflect production

* Fri Apr 17 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 2.8.13-2
- reorder some menu entries, small fixes

* Fri Apr 17 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 2.8.13-1
- make C81 production

* Wed Apr 15 2020 Ben Morrice <ben.morrice@cern.ch> - 2.8.12-1
- add CC78 TEST menus

* Tue Apr 07 2020 Ben Morrice <ben.morrice@cern.ch> - 2.8.11-1
- add RHEL 7.8 menus

* Tue Mar 31 2020 Daniel Juarez <djuarezg@cern.ch> - 2.8.10-2
- change C8 url match CC7 naming convention

* Thu Feb 27 2020 Ben Morrice <ben.morrice@cern.ch> - 2.8.10-1
- change C8 url to use 'os' instead of 'kickstart'

* Thu Feb 27 2020 Ben Morrice <ben.morrice@cern.ch> - 2.8.9-1
- add locmap via inst.repo to C8 menus

* Thu Jan 30 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 2.8.8-1
- add C8.1 to BE/CO menu

* Fri Jan 17 2020 Ben Morrice <ben.morrice@cern.ch> - 2.8.7-1
- add C8.1 menu

* Wed Dec 11 2019 Ben Morrice <ben.morrice@cern.ch> - 2.8.6-1
- refactor UEFI menu
- add RHEL 8.1

* Wed Dec 11 2019 Ben Morrice <ben.morrice@cern.ch> - 2.8.5-1
- change C8 TEST URLs to point to 8.0

* Wed Dec 04 2019 Ben Morrice <ben.morrice@cern.ch> - 2.8.4-1
- add C8 TEST menus

* Fri Nov 01 2019 Daniel Juarez <djuarezg@cern.ch> - 2.8.3-3
- UEFI images are now provided through HTTP

* Wed Oct 23 2019 Daniel Juarez <djuarezg@cern.ch> - 2.8.3-2
- add UEFI support for unregistered machines

* Wed Oct 23 2019 Daniel Juarez <djuarezg@cern.ch> - 2.8.2-11
- add rpmci

* Fri Oct 18 2019 Ben Morrice <ben.morrice@cern.ch> - 2.8.2-9
- add extra BE/CO menus (RQF1437862)

* Mon Sep 30 2019 Ben Morrice <ben.morrice@cern.ch> - 2.8.2-8
- add  CC 7.7 PROD (removing TEST)

* Tue Sep 17 2019 Ben Morrice <ben.morrice@cern.ch> - 2.8.2-7
- add  CC 7.7 TEST

* Tue Aug 13 2019 Ben Morrice <ben.morrice@cern.ch> - 2.8.2-6
- add  RHEL 7.7

* Tue Aug 06 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.8.2-5
- add UEFI FEDORA 30

* Mon Jul 22 2019 Ben Morrice <ben.morrice@cern.ch> - 2.8.2-4
 - add Fedora 30
 - remove Fedora 27

* Wed May 08 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.8.2-3
- add FEDORA 29

* Wed May 08 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.8.2-1
- add RHEL 8.0 to menu
- remove RHEL 8.0 Beta 1 to menu

* Mon Apr 08 2019 Ulrich Schwickerath <ulrich.schwickerath@cern.ch> - 2.8.1-1
- update grub.cfg for arm64 to support mellanox network cards
- remove progress module which does not seem to work correctly for arm64

* Mon Apr 01 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.8.0-1
- Remove SLC5

* Mon Feb 04 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.7.3-3
- Modify BE-CO menu

* Mon Dec 10 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.7.3
- add CERN CentOS 7 (7.6)

* Fri Dec 07 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.7.2
- add CentOS 7.6 altarch-aarch64 to menu.

* Mon Nov 26 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.7
- add CERN CentOS 7.6 to menu
- add post code.

* Fri Nov 16 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.6
- add RHEL 8.0 Beta 1 to menu.

* Thu Nov 08 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.5
- add RHEL 7.6 to menu.

* Fri Sep 28 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.2
- tools menu fixes.

* Wed Sep 26 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.1
- unify bios/lgcy menus regeneration.

* Wed Aug 15 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.0
- regenerate menus from templates on build.
- identify server name in display.
- menus cleanup.
- per server setup

* Tue Aug 14 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 1.1
- reconfigure on dest system
- fix bios menus.
* Wed Aug 08 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 1.0
- initial package

