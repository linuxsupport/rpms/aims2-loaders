include @AIMS_CONFIG_PATH@/graphics@MENU_TEXT@.conf

allowoptions 1

menu title BE/CO System Installation Menu
F1 @AIMS_CONFIG_PATH@/f1help_expert.txt

label beco_CC79_X86_64
 menu label Install CERN CentOS 7.9 (CC7.9/X86_64)
 kernel @AIMS_BOOT_PATH@/CC79_X86_64/vmlinuz
 append text initrd=@AIMS_BOOT_PATH@/CC79_X86_64/initrd ip=dhcp nofb ksdevice=bootif biosdevname=0 net.ifnames=0 ks=nfs:cs-ccr-accadm:/opt/accadm/kickstart/
 ipappend 2
  text help
  CERN CentOS 7 64-bit BE/CO installation.
 endtext

label beco_CC78_X86_64
 menu label Install CERN CentOS 7.8 (CC7.8/X86_64)
 kernel @AIMS_BOOT_PATH@/CC78_X86_64/vmlinuz
 append text initrd=@AIMS_BOOT_PATH@/CC78_X86_64/initrd ip=dhcp nofb ksdevice=bootif biosdevname=0 net.ifnames=0 ks=nfs:cs-ccr-accadm:/opt/accadm/kickstart/
 ipappend 2
  text help
  CERN CentOS 7 64-bit BE/CO installation.
 endtext

label beco_CC77_X86_64
 menu label Install CERN CentOS 7.7 (CC7.7/X86_64)
 kernel @AIMS_BOOT_PATH@/CC77_X86_64/vmlinuz
 append text initrd=@AIMS_BOOT_PATH@/CC77_X86_64/initrd ip=dhcp nofb ksdevice=bootif biosdevname=0 net.ifnames=0 ks=nfs:cs-ccr-accadm:/opt/accadm/kickstart/
 ipappend 2
  text help
  CERN CentOS 7 64-bit BE/CO installation.
 endtext

label beco_CC76_X86_64
 menu label Install CERN CentOS 7.6 (CC7.6/X86_64)
 kernel @AIMS_BOOT_PATH@/CC76_X86_64/vmlinuz
 append text initrd=@AIMS_BOOT_PATH@/CC76_X86_64/initrd ip=dhcp nofb ksdevice=bootif biosdevname=0 net.ifnames=0 ks=nfs:cs-ccr-accadm:/opt/accadm/kickstart/
 ipappend 2
  text help
  CERN CentOS 7 64-bit BE/CO installation.
 endtext

label beco_SLC69_x86_64
 menu label Install Scientific Linux CERN 6.9 (SLC6.9/x86_64)
 kernel @AIMS_BOOT_PATH@/SLC69_X86_64/vmlinuz
 append text initrd=@AIMS_BOOT_PATH@/SLC69_X86_64/initrd ip=dhcp ksdevice=bootif nofb noipv6 ks=nfs:cs-ccr-accadm:/opt/accadm/kickstart/
 ipappend 2
 text help
  Scientific Linux CERN 6 64-bit BE/CO installation.
 endtext

label gotodefault
 menu label Return to Main Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/main@MENU_TEXT@.conf
 text help
 Return to the main installation menu.
 endtext

label fromhd
 menu label Boot system from local disk
 menu default
 localboot 0
 text help
  This menu option will boot your system from its local
  disk (or any other local boot device)
 endtext

