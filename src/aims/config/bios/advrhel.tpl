include @AIMS_CONFIG_PATH@/graphics@MENU_TEXT@.conf

allowoptions 1

menu title Red Hat Enterprise Linux installation (LICENSE REQUIRED)
F1 @AIMS_CONFIG_PATH@/f1help_expert.txt

label rhel83_x86_64
  menu label Install RHEL 8.3 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_8_3_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_8_3_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.3/x86_64/
 text help
  Install Red Hat Enteprise Linux 8.3 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel82_x86_64
  menu label Install RHEL 8.2 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_8_2_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_8_2_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.2/x86_64/
 text help
  Install Red Hat Enteprise Linux 8.2 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel81_x86_64
  menu label Install RHEL 8.1 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_8_1_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_8_1_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.1/x86_64/
 text help
  Install Red Hat Enteprise Linux 8.1 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel80_x86_64
  menu label Install RHEL 8.0 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_8_0_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_8_0_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.0/x86_64/
 text help
  Install Red Hat Enteprise Linux 8.0 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel79_x86_64
 menu label Install RHEL 7.9 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_7_9_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_7_9_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.9/x86_64/
 text help
  Install Red Hat Enteprise Linux 7.9 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel78_x86_64
 menu label Install RHEL 7.8 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_7_8_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_7_8_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.8/x86_64/
 text help
  Install Red Hat Enteprise Linux 7.8 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel77_x86_64
 menu label Install RHEL 7.7 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_7_7_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_7_7_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.7/x86_64/
 text help
  Install Red Hat Enteprise Linux 7.7 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel76_x86_64
 menu label Install RHEL 7.6 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_7_6_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_7_6_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.6/x86_64/
 text help
  Install Red Hat Enteprise Linux 7.6 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel75_x86_64
 menu label Install RHEL 7.5 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_7_5_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_7_5_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.5/x86_64/
 text help
  Install Red Hat Enteprise Linux 7.5 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel74_x86_64
 menu label Install RHEL 7.4 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_7_4_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_7_4_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.4/x86_64/
 text help
  Install Red Hat Enteprise Linux 7.4 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel610_x86_64
 menu label Install RHEL 6.10 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_6_10_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_6_10_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.10/x86_64/
 text help
  Install Red Hat Enteprise Linux 6.10 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel610_i386
 menu label Install RHEL 6.10 Server 32-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_6_10_I386/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_6_10_I386/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.10/i386/
 text help
  Install Red Hat Enteprise Linux 6.10 Server 32-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel69_x86_64
 menu label Install RHEL 6.9 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_6_9_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_6_9_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.9/x86_64/
 text help
  Install Red Hat Enteprise Linux 6.9 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel69_i386
 menu label Install RHEL 6.9 Server 32-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_6_9_I386/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_6_9_I386/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.9/i386/
 text help
  Install Red Hat Enteprise Linux 6.9 Server 32-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label rhel511_x86_64
 menu label Install RHEL 5.11 Server 64-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_5_11_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_5_11_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/5/5.11/x86_64/
 text help
  Install Red Hat Enteprise Linux 5.11 Server 64-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext
 
label rhel511_i386
 menu label Install RHEL 5.11 Server 32-bit (LICENSE REQUIRED)
 kernel @AIMS_BOOT_PATH@/RHEL_5_11_I386/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/RHEL_5_11_I386/initrd ip=dhcp method=http://linuxsoft.cern.ch/enterprise/rhel/server/5/5.11/i386/
 text help
  Install Red Hat Enteprise Linux 5.11 Server 32-bit version.
  Please note: Valid Red Hat LICENSE is REQUIRED.
 endtext

label gotodefault
 menu label Return to Main Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/main@MENU_TEXT@.conf
 text help
 Return to the main installation menu.
 endtext

label fromhd
 menu label Boot system from local disk
 menu default
 localboot 0
 text help
  This menu option will boot your system from its local
  disk (or any other local boot device).
 endtext


