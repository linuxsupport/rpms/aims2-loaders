include @AIMS_CONFIG_PATH@/graphics@MENU_TEXT@.conf

allowoptions 1

menu title CERN CentOS (CC) / Scientific Linux CERN (SLC) Install
F1 @AIMS_CONFIG_PATH@/f1help_expert.txt

label cs8_2021Q2_X86_64
 menu label Install CentOS Stream 8 Linux for 20201Q2 (CS8_2021Q2) 64-bit system.
 kernel @AIMS_BOOT_PATH@/CS82021.2_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CS82021.2_X86_64/initrd ip=dhcp inst.repo=http://linuxsoft.cern.ch/cern/centos/s8.2021.2/BaseOS/x86_64/os inst.addrepo=CERN,http://linuxsoft.cern.ch/cern/centos/s8.2021.2/CERN/x86_64/ inst.addrepo=locmap,http://linuxsoft.cern.ch/internal/repos/potd8s-stable/x86_64/os/ ks=http://linuxsoft.cern.ch/kickstart/centos8_default.ks
 text help
  Install CS8 64-bit linux on your
  system. For more information, please see:
  http://cern.ch/linux/centos8/
 endtext

label c8X_x86_64
 menu label Install CentOS ^8 Linux (C8) 64-bit system.
 kernel @AIMS_BOOT_PATH@/C8_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/C8_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/cern/centos/8/BaseOS/x86_64/kickstart/ inst.addrepo=CERN,http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/ inst.addrepo=locmap,http://linuxsoft.cern.ch/internal/repos/potd8-stable/x86_64/os/ ks=http://linuxsoft.cern.ch/kickstart/centos8_default.ks
 text help
  Install C8 64-bit linux on your
  system. For more information, please see:
  http://cern.ch/linux/centos8/
 endtext

label c82_x86_64
 menu label Install CentOS ^8 Linux (C8.2) 64-bit system.
 kernel @AIMS_BOOT_PATH@/C82_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/C82_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/cern/centos/8.2/BaseOS/x86_64/kickstart/ inst.addrepo=CERN,http://linuxsoft.cern.ch/cern/centos/8.2/CERN/x86_64/ inst.addrepo=locmap,http://linuxsoft.cern.ch/internal/repos/potd8-stable/x86_64/os/ ks=http://linuxsoft.cern.ch/kickstart/centos8_default.ks
 text help
  Install C8 64-bit linux on your
  system. For more information, please see:
  http://cern.ch/linux/centos8/
 endtext

label c81_x86_64
 menu label Install CentOS ^8 Linux (C8.1) 64-bit system.
 kernel @AIMS_BOOT_PATH@/C81_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/C81_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/cern/centos/8.1/BaseOS/x86_64/kickstart/ inst.addrepo=CERN,http://linuxsoft.cern.ch/cern/centos/8.1/CERN/x86_64/ inst.addrepo=locmap,http://linuxsoft.cern.ch/internal/repos/potd8-stable/x86_64/os/ ks=http://linuxsoft.cern.ch/kickstart/centos8_default.ks
 text help
  Install C8 64-bit linux on your
  system. For more information, please see:
  http://cern.ch/linux/centos8/
 endtext

label cc7X_x86_64
 menu label Install CERN CentOS ^7 Linux (CC7) 64-bit system.
 kernel @AIMS_BOOT_PATH@/CC7_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CC7_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/
 text help
 Install supported version of CERN CentOS 7 64-bit
 on your system. For more information, please see:
  http://cern.ch/linux/centos7/
 endtext

label cc7X_x86_64_amd
 menu label Install CERN CentOS ^7 Linux (CC7) - AMD workaround.
 kernel @AIMS_BOOT_PATH@/CC7_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CC7_X86_64/initrd ip=dhcp initcall_blacklist=clocksource_done_booting repo=http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/
 text help
 Install supported version of CERN CentOS 7 64-bit
 on your system. For more information, please see:
  http://cern.ch/linux/centos7/
 endtext

label cc79_x86_64
 menu label Install CERN CentOS ^7 Linux (CC7.9) 64-bit system.
 kernel @AIMS_BOOT_PATH@/CC79_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CC79_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/
 text help
 Install supported version of CERN CentOS 7 64-bit
 on your system. For more information, please see:
  http://cern.ch/linux/centos7/
 endtext

label cc78_x86_64
 menu label Install CERN CentOS ^7 Linux (CC7.8) 64-bit system.
 kernel @AIMS_BOOT_PATH@/CC78_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CC78_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/
 text help
 Install supported version of CERN CentOS 7 64-bit
 on your system. For more information, please see:
  http://cern.ch/linux/centos7/
 endtext

label cc77_x86_64
 menu label Install CERN CentOS ^7 Linux (CC7.7) 64-bit system.
 kernel @AIMS_BOOT_PATH@/CC77_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CC77_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/
 text help
 Install supported version of CERN CentOS 7 64-bit
 on your system. For more information, please see:
  http://cern.ch/linux/centos7/
 endtext

label cc76_x86_64
 menu label Install CERN CentOS ^7 Linux (CC7.6) 64-bit system.
 kernel @AIMS_BOOT_PATH@/CC76_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CC76_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/
 text help
 Install supported version of CERN CentOS 7 64-bit
 on your system. For more information, please see:
  http://cern.ch/linux/centos7/
 endtext

label slc6X_x86_64
 menu label Install SL CERN ^6 (SLC6) 64-bit (Unsupported)
 kernel @AIMS_BOOT_PATH@/SLC6X_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/SLC6X_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/slc6X/x86_64/
text help
 Install CERN unsupported 64-bit linux on your
 system. Unsupported, see OTG0054345. For more information, please see:
  http://cern.ch/linux/scientific6/
 endtext

label slc6X_i386
 menu label Install SL CERN 6 (SLC6) 32-bit (Unsupported)
 kernel @AIMS_BOOT_PATH@/SLC6X_I386/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/SLC6X_I386/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/slc6X/i386/
text help
 Install CERN unsupported 32-bit linux on your
 system. Unsupported, see OTG0054345. For more information, please see:
  http://cern.ch/linux/scientific6/
 endtext

label slc69_x86_64
 menu label Install SL CERN 6.9 (SLC6.9) 64-bit (Unsupported)
 kernel @AIMS_BOOT_PATH@/SLC69_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/SLC69_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/slc69/x86_64/
text help
 Install CERN unsupported 64-bit linux on your
 system. Unsupported, see OTG0054345. For more information, please see:
  http://cern.ch/linux/scientific6/
 endtext

label slc69_i386
 menu label Install SL CERN 6.9 (SLC6.9) 32-bit (Unsupported)
 kernel @AIMS_BOOT_PATH@/SLC69_I386/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/SLC69_I386/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/slc69/i386/
text help
 Install CERN unsupported 32-bit linux on your
 system. Unsupported, see OTG0054345. For more information, please see:
  http://cern.ch/linux/scientific6/
 endtext

label slc68_i386_nonpae
  menu label Install SL CERN 6.8 NONPAE (SLC6.8) 32-bit (Unsupported)
  kernel @AIMS_BOOT_PATH@/SLC68_I386_NONPAE/vmlinuz
  append initrd=@AIMS_BOOT_PATH@/SLC68_I386_NONPAE/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/slc68/i386nonpae/
text help
 Install CERN Non-PAE 32-bit linux on your
 system. Unsupported, see OTG0054345. For more information, please see:
  http://cern.ch/linux/scientific6/
 endtext

label gotodefault
 menu label Return to Main Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/main@MENU_TEXT@.conf
 text help
 Return to the main installation menu.
 endtext

label fromhd
 menu label Boot system from local disk
 menu default
 localboot 0
 text help
  This menu option will boot your system from its local
  disk (or any other local boot device)
 endtext

