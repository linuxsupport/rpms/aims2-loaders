serial 0

default @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
prompt 0

@VESA_BACKGROUND_FILE@menu background @AIMS_LOADER_PATH@/unibackgrnd.png
menu margin 10
menu vshift 6
menu hshift 6
menu rows 10
menu width 80
menu timeoutrow 17
menu tabmsgrow 18
menu cmdlinerow 19
menu endrow -1
menu helpmsgrow 20
menu helpmsgendrow -1
menu notabmsg              Press [F1] for help. (AIMS: @AIMS_SERVER@)
menu tabmsg Press [Tab] to edit, [F1] for help. (AIMS: @AIMS_SERVER@)
menu clear
