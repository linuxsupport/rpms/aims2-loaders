default @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
include @AIMS_CONFIG_PATH@/graphics@MENU_TEXT@.conf
timeout 150
allowoptions 0

menu title Select Operating System to install
F1 @AIMS_CONFIG_PATH@/f1help_default.txt

label c8_x86_64
 menu label Install CentOS ^8 Linux (C8) 64-bit system.
 kernel @AIMS_BOOT_PATH@/C8_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/C8_X86_64/initrd ip=dhcp inst.repo=http://linuxsoft.cern.ch/cern/centos/8/BaseOS/x86_64/os inst.addrepo=CERN,http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/ inst.addrepo=locmap,http://linuxsoft.cern.ch/internal/repos/potd8-stable/x86_64/os/ ks=http://linuxsoft.cern.ch/kickstart/centos8_default.ks
 text help
  Install C8 64-bit linux on your
  system. For more information, please see:
  http://cern.ch/linux/centos8/
 endtext

label cs8_x86_64
 menu label Install CentOS Stream ^8 Linux (CS8) 64-bit system.
 kernel @AIMS_BOOT_PATH@/CS8_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CS8_X86_64/initrd ip=dhcp inst.repo=http://linuxsoft.cern.ch/cern/centos/s8/BaseOS/x86_64/os inst.addrepo=CERN,http://linuxsoft.cern.ch/cern/centos/s8/CERN/x86_64/ inst.addrepo=locmap,http://linuxsoft.cern.ch/internal/repos/potd8s-stable/x86_64/os/ ks=http://linuxsoft.cern.ch/kickstart/centos8_default.ks
 text help
  Install CS8 64-bit linux on your
  system. For more information, please see:
  http://cern.ch/linux/centos8/
 endtext

label cc7_x86_64
 menu label Install CERN CentOS ^7 Linux (CC7) 64-bit system.
 kernel @AIMS_BOOT_PATH@/CC7_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CC7_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/
 text help
  Install CERN supported 64-bit linux on your
  system. For more information, please see:
  http://cern.ch/linux/centos7/
 endtext

label fromhd
 menu label Boot system from local disk
 menu default
 localboot 5
 text help
  This menu option will boot your system from its local
  disk (or any other local boot device).
 endtext

label expert
 menu label Expert Operating System Install Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/expert@MENU_TEXT@.conf
 text help
  Expert Install Menu will allow you to edit installation options for
  your system. Expert Menu also contains submenus for experiment / group
  installation setups and other (unsupported) operating systems.
 endtext

label netbootxyz
 menu label Other Operating Systems (Unsupported, External)
 kernel http://boot.netboot.xyz/ipxe/netboot.xyz.lkrn
 append @AIMS_CONFIG_PATH@/expert@MENU_TEXT@.conf
 text help
  This options uses netboot.xyz which provides a huge variety of distributions. Complete list
  can be found on https://netboot.xyz/faq/. These distributions are not supported by CERN.
  Use at your own discretion.
 endtext

label tools
 menu label Rescue / Tools Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/tools@MENU_TEXT@.conf
 text help
  Rescue / Tools Menu provides rescue boot images for your linux system
  and also some diagnostic tools.
 endtext

